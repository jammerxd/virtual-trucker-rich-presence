﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient
{
    public interface IETCARSClient :IDisposable
    {
        string GetId();
        void OnReceive(string data);
    }
}
