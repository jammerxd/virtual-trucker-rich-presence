﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ETCARSClient
{
    public class ETCARSClient
    {

        private Socket client;
        private int port=30000;
        private string hostEntry="localhost";
        public bool Connected { get; set; }

        public List<IETCARSClient> Callbacks;
        public static int CallbackId;
        public ETCARSClient()
        {
            Callbacks = new List<IETCARSClient>();
            CallbackId = -1;
        }

        public int AddCallback(IETCARSClient cl)
        {
            CallbackId++;

            if (!Callbacks.Select(x => x.GetId()).Contains(cl.GetId()))
            {
                Callbacks.Add(cl);

                return CallbackId;
            }
            else
            {
                return -1;
            }
        }

        public void RemoveCallback(string id)
        {
            Callbacks.RemoveAt(Callbacks.FindIndex(x => x.GetId() == id));

        }

        public bool Connect()
        {
            try
            {
                AsynchronousClient.AddReceiveCallback(new ClientReceiveCallback(this.Receive, true));
                client = AsynchronousClient.Connect(hostEntry, port);
                Connected = true;
                return true;
            }
            catch(ClientException ex)
            {
                throw;
            }
        }

        public bool Disconnect()
        {
            try
            {
                AsynchronousClient.Disconnect(client);
                Connected = false;
                return true;
            }
            catch(ClientException ex)
            {
                Connected = false;
                throw;
            }
        }

        public void Receive(string data)
        {
            int startIndex = data.IndexOf("{");
            int endIndex = data.LastIndexOf("}");
            int length = (endIndex - startIndex)+1;
            string json = data.Substring(startIndex, length);
            foreach(IETCARSClient receiver in Callbacks)
            {
                Task.Run(() => receiver.OnReceive(json));
            }

        }
       
        
    }
}
