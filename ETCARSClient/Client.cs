﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public enum CallbackMethod
{
    OnConnect=1,
    OnDisconnect=2,
    OnReceive=3,
    OnSend
}
public interface IClientCallback
{
    void OnConnect();
    void OnDisconnect();
    void OnReceive(string data);
    void OnSend();
}
public class ClientCallback
{
    private static int id = 1;
    public int Id { get; private set; }
    public IClientCallback Callback { get; set; }
    public bool Enabled { get; set; }

    public ClientCallback(bool enabled = true) { this.Id = id; id++; this.Enabled = enabled; }
    public ClientCallback(IClientCallback callback,bool enabled = true) { this.Id = id;id++;this.Callback = callback; this.Enabled = enabled; }
}
public class ClientReceiveCallback
{
    private static int id = 1;
    public int Id { get; private set; }
    public Action<string> Callback { get; set; }
    public bool Enabled { get; set; }
    public ClientReceiveCallback(bool enabled = true) { this.Id = id;id++;this.Enabled = enabled; }
    public ClientReceiveCallback(Action<string> action,bool enabled = true) { this.Id = id;id++;this.Enabled = enabled;this.Callback = action; }
}
public class ClientException : Exception
{
    public string ETCARSMessage { get; set; }
    public Exception Exception { get; set; }
    public ClientException() { }
    public ClientException(Exception ex=null,string ETCARSMessage = "")
    {
        
        this.ETCARSMessage = ETCARSMessage;
        this.Exception = ex;
        
    }
}

// State object for receiving data from remote device.  
public class StateObject
{
    // Client socket.  
    public Socket workSocket = null;
    // Size of receive buffer.  
    public const int BufferSize = 256;
    // Receive buffer.  
    public byte[] buffer = new byte[BufferSize];
    // Received data string.  
    public StringBuilder sb = new StringBuilder();
}

public class AsynchronousClient
{
    private static List<ClientCallback> Callbacks { get; set; }
    private static List<ClientReceiveCallback> ReceiveCallbacks { get; set; }
    private static object CallbacksLock { get; set; }
    private static object ReceiveCallbacksLock { get; set; }
        

    // ManualResetEvent instances signal completion.  
    private static ManualResetEvent connectDone =
        new ManualResetEvent(false);
    private static ManualResetEvent sendDone =
        new ManualResetEvent(false);
    private static ManualResetEvent receiveDone =
        new ManualResetEvent(false);
    private static ManualResetEvent disconnectDone =
        new ManualResetEvent(false);


    public AsynchronousClient() { }
    static AsynchronousClient() 
    {
        Callbacks = new List<ClientCallback>();
        ReceiveCallbacks = new List<ClientReceiveCallback>();
    }

    public static void AddCallback(ClientCallback callback)
    {
        lock (CallbacksLock)
        {
            Callbacks.Add(callback);
        }
        
    }
    public static void RemoveCallback(int id)
    {
        lock(CallbacksLock)
        {
            Callbacks.Remove(Callbacks.Where(x => x.Id == id).FirstOrDefault());
        }
    }
    public static void RemoveCallback(ClientCallback callback)
    {
        lock(CallbacksLock)
        {
            Callbacks.Remove(Callbacks.Where(x => x.Id == callback.Id).FirstOrDefault());
        }
    }

    public static void AddReceiveCallback(ClientReceiveCallback callback)
    {
        lock (ReceiveCallbacksLock)
        {
            ReceiveCallbacks.Add(callback);
        }

    }
    public static void RemoveReceiveCallback(int id)
    {
        lock (ReceiveCallbacksLock)
        {
            ReceiveCallbacks.Remove(ReceiveCallbacks.Where(x => x.Id == id).FirstOrDefault());
        }
    }
    public static void RemoveReceiveCallback(ClientReceiveCallback callback)
    {
        lock (ReceiveCallbacksLock)
        {
            ReceiveCallbacks.Remove(ReceiveCallbacks.Where(x => x.Id == callback.Id).FirstOrDefault());
        }
    }

    private static void DoCallbacks(CallbackMethod EMethod,string data)
    {
        lock (CallbacksLock)
        {
            foreach (var callback in Callbacks.Where(x=>x.Enabled))
            {
                
                if(EMethod == CallbackMethod.OnConnect)
                {
                    Task.Run(callback.Callback.OnConnect);
                }
                else if(EMethod == CallbackMethod.OnDisconnect)
                {
                    Task.Run(callback.Callback.OnConnect);
                }
                else if(EMethod == CallbackMethod.OnReceive)
                {
                    Task.Run(()=>callback.Callback.OnReceive(data));
                }
            }
        }
    }
    private static void DoCallbacks(string data)
    {
        lock (ReceiveCallbacksLock)
        {
            foreach (var callback in ReceiveCallbacks.Where(x => x.Enabled))
            {
                Task.Run(()=>callback.Callback(data));
            }
        }
    }
    public static Socket Connect(string hostEntry="localhost", int port=30000)
    {
        
        // Connect to a remote device.  
        try
        {
            
            // Establish the remote endpoint for the socket.  
            // The name of the   
            // remote device is "host.contoso.com".  
            IPHostEntry ipHostInfo = Dns.GetHostEntry(hostEntry);
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

            // Create a TCP/IP socket.  
            Socket client = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            // Connect to the remote endpoint.  
            client.BeginConnect(remoteEP,
                new AsyncCallback(ConnectCallback), client);
            connectDone.WaitOne();

            Task.Run(() => { DoCallbacks(CallbackMethod.OnConnect,string.Empty); });

            // Send test data to the remote device.  
            //Send(client, "This is a test<EOF>");
            //sendDone.WaitOne();

            // Receive the response from the remote device.  
            //Receive(client);
            //receiveDone.WaitOne();

            // Write the response to the console.  
            //Console.WriteLine("Response received : {0}", response);

            // Release the socket.  
            //client.Shutdown(SocketShutdown.Both);
            //client.Close();
            return client;

        }
        catch (Exception ex)
        {
            throw new ClientException(ex,"Exception starting client.");
        }
    }

    private static void ConnectCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the socket from the state object.  
            Socket client = (Socket)ar.AsyncState;

            // Complete the connection.  
            client.EndConnect(ar);

            //Console.WriteLine("Socket connected to {0}",
            //    client.RemoteEndPoint.ToString());

            // Signal that the connection has been made.  
            connectDone.Set();
        }
        catch (Exception ex)
        {
            throw new ClientException(ex, "Exception completing connection[Connection Callback]");
        }
    }

    public static void Disconnect(Socket client)
    {
        try
        {
            StateObject state = new StateObject();
            state.workSocket = client;
            client.BeginDisconnect(true, new AsyncCallback(DisconnectCallback), state);
            disconnectDone.WaitOne();
            Task.Run(() => { DoCallbacks(CallbackMethod.OnDisconnect,string.Empty); });
        }
        catch(Exception ex)
        {
            throw new ClientException(ex, "Exception disconnecting");
        }

    }
    private static void DisconnectCallback(IAsyncResult ar)
    {
        try
        {
            Socket client = (Socket)ar.AsyncState;

            client.EndDisconnect(ar);
            client.Shutdown(SocketShutdown.Both);
            client.Close();
            client.Dispose();
            disconnectDone.Set();
        }
        catch (Exception ex)
        {
            throw new ClientException(ex, "Exception completeing disconnect[Disconnect Callback]");
        }
    }

    private static void Receive(Socket client)
    {
        try
        {
            // Create the state object.  
            StateObject state = new StateObject();
            state.workSocket = client;

            // Begin receiving the data from the remote device.  
            client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReceiveCallback), state);

            receiveDone.WaitOne();
            Task.Run(() => DoCallbacks(CallbackMethod.OnReceive,state.sb.ToString()));
            Task.Run(() => DoCallbacks(state.sb.ToString()));
        }
        catch (Exception ex)
        {
            throw new ClientException(ex, "Exception receiving data");
        }
    }

    private static void ReceiveCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the state object and the client socket   
            // from the asynchronous state object.  
            StateObject state = (StateObject)ar.AsyncState;
            Socket client = state.workSocket;

            // Read data from the remote device.  
            int bytesRead = client.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There might be more data, so store the data received so far.  
                state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, bytesRead));

                // Get the rest of the data.  
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            else
            {
                // All the data has arrived; put it in response.  
                //if (state.sb.Length > 1)
                //{
                //    response = state.sb.ToString();
                //}
                // Signal that all bytes have been received.  
                receiveDone.Set();
            }
        }
        catch (Exception ex)
        {
            throw new ClientException(ex, "Exception receiving data[Receive Callback]");
        }
    }

    public static void Send(Socket client, String data)
    {
        // Convert the string data to byte data using ASCII encoding.  
        byte[] byteData = Encoding.UTF8.GetBytes(data);

        // Begin sending the data to the remote device.  
        client.BeginSend(byteData, 0, byteData.Length, 0,
            new AsyncCallback(SendCallback), client);

        sendDone.WaitOne();
        Task.Run(() => DoCallbacks(CallbackMethod.OnSend,string.Empty));
    }

    private static void SendCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the socket from the state object.  
            Socket client = (Socket)ar.AsyncState;

            // Complete sending the data to the remote device.  
            int bytesSent = client.EndSend(ar);
            //Console.WriteLine("Sent {0} bytes to server.", bytesSent);

            // Signal that all bytes have been sent.  
            sendDone.Set();
        }
        catch (Exception ex)
        {
            throw new ClientException(ex, "Exception in sending data[Send Callback]");
        }
    }



}