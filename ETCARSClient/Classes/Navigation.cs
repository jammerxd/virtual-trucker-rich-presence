﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Navigation
    {
        public float distance { get; set; }
        public float time { get; set; }
        public float lowestDistance { get;set; }
        public float speedLimit { get; set; }
    }
}
