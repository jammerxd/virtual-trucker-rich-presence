﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class DLC
    {
        public int appid { get; set; }
        public string name { get; set; }
        public bool available { get; set; }
        public bool installed { get; set; }
    }
}
