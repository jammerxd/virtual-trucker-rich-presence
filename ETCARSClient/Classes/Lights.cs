﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Lights
    {
        public bool lowBeam { get; set; }
        public bool highBeam { get; set; }
        public int frontAux { get; set; }
        public bool beacon { get; set; }
        public bool parking { get; set; }
        public bool brake { get; set; }
        public bool reverse { get; set; }
        public bool leftBlinkerEnabled { get; set; }
        public bool rightBlinkerEnabled { get; set; }
        public bool leftBlinkerOn { get; set; }
        public bool rightBlinkerOn { get; set; }
        public int roofAux { get; set; }
    }
}
