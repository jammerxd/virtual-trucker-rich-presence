﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Data
    {
        public Telemetry data { get; set; }
        public Data()
        {
            this.data = new Telemetry();
        }
    }
}
