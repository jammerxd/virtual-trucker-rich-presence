﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Fuel
    {
        public float capacity { get; set; }
        public float warningLevel { get; set; }
        public float consumptionAverage { get; set; }
        public float currentLitres { get; set; }
    }
}
