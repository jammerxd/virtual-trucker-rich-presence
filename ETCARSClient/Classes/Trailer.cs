﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Trailer
    {
        public Placement linearVelocity { get; set; }
        public Placement angularVelocity { get; set; }
        public Placement linearAcceleration { get; set; }
        public Placement angularAcceleration { get; set; }
        public int wheelCount { get; set; }
        public List<WheelInfo> wheelInfo { get; set; }
        public string id { get; set; }
        public string cargoAccessoryId { get; set; }
        public Placement hookPosition { get; set; }

        public Trailer()
        {
            this.angularAcceleration = new Placement();
            this.angularVelocity = new Placement();
            this.hookPosition = new Placement();
            this.linearAcceleration = new Placement();
            this.linearVelocity = new Placement();
            this.wheelInfo = new List<WheelInfo>();
        }
    }
}
