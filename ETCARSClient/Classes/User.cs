﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class User
    {
        public string steamID { get; set; }
        public string steamUsername { get; set; }
        public DLCContainer DLC { get; set; }
        public User()
        {
            this.DLC = new DLCContainer();
        }
    }
}
