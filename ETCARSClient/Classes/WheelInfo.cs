﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class WheelInfo
    {
        public float suspensionDeflection { get; set; }
        public bool onGround { get; set; }
        public string substance { get; set; }
        public float angularVelocity { get; set; }
        public float lift { get; set; }
        public float liftOffset { get; set; }
        public Placement position { get; set; }
        public bool steerable { get; set; }
        public bool simulated { get; set; }
        public float radius { get; set; }
        public float steering { get; set; }
        public float rotation { get; set; }
        public bool powered { get; set; }
        public bool liftable { get; set; }

        public WheelInfo()
        {
            this.position = new Placement();
        }
    }
}
