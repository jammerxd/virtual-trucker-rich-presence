﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Warnings
    {

        public bool batterVoltage { get; set; }
        public bool airPressure { get; set; }
        public bool airPressureEmergency { get; set; }
        public bool oilPressure { get; set; }
        public bool waterTemperature { get; set; }
        public bool fuelLow { get; set; }
        public bool adBlue { get; set; }
    }
}
