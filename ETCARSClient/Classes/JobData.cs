﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class JobData
    {
        public int status { get; set; }
        public bool wasSpeeding { get; set; }
        public bool jobStartedEventFired { get; set; }
        public bool isMultiplayer { get; set; }
        public bool late { get; set; }
        public bool onJob { get; set; }
        public bool wasFinished { get; set; }
        public bool wasTrailerDisconnected { get; set; }
        public string cargoID { get; set; }
        public string cargo { get; set; }
        public float trailerMass { get; set; }
        public float income { get; set; }
        public string destinationCityID { get; set; }
        public string destinationCity { get; set; }
        public string destinationCompanyID { get; set; }
        public string destinationCompany { get; set; }
        public string sourceCityID { get; set; }
        public string sourceCity { get; set; }
        public string sourceCompanyID { get; set; }
        public string sourceCompany { get; set; }
        public int deliveryTime { get; set; }
        public bool isLate { get; set; }
        public int timeRemaining { get; set; }
        public string truckMake { get; set; }
        public string truckMakeID { get; set; }
        public string truckModel { get; set; }
        public string truckModelID { get; set; }
        public string gameID { get; set; }
        public string game { get; set; }
        public string gameVersion { get; set; }
        public string pluginVersion { get; set; }
        public float topSpeed { get; set; }
        public int speedingCount { get; set; }
        public float distanceDriven { get; set; }
        public float fuelBurned { get; set; }
        public float fuelPurchased { get; set; }
        public float startOdometer { get; set; }
        public float endOdometer { get; set; }
        public long timeStarted { get; set; }
        public long timeDue { get; set; }
        public long timeDelivered { get; set; }
        public int collisionCount { get; set; }
        public float finishTrailerDamage { get; set; }
        public float startTrailerDamage { get; set; }
        public float deliveryX { get; set; }
        public float deliveryY { get; set; }
        public float deliveryZ { get; set; }
        public float pickupX { get; set; }
        public float pickupY { get; set; }
        public float pickupZ { get; set; }
        public float trailerDeliveryX { get; set; }
        public float trailerDeliveryY { get; set; }
        public float trailerDeliveryZ { get; set; }
        public float trailerPickupX { get; set; }
        public float trailerPickupY { get; set; }
        public float trailerPickupZ { get; set; }
        public float startEngineDamage { get; set; }
        public float startTransmissionDamage { get; set; }
        public float startCabinDamage { get; set; }
        public float startChassisDamage { get; set; }
        public float startWheelDamage { get; set; }
        public float finishEngineDamage { get; set; }
        public float finishTransmissionDamage { get; set; }
        public float finishCabinDamage { get; set; }
        public float finishChassisDamage { get; set; }
        public float finishWheelDamage { get; set; }
        public float totalEngineDamage { get; set; }
        public float totalTransmissionDamage { get; set; }
        public float totalCabinDamage { get; set; }
        public float totalChassisDamage { get; set; }
        public float totalWheelDamage { get; set; }
        public float totalTrailerDamage { get; set; }
        public float navigationDistanceRemaining { get; set; }
        public float fuel { get; set; }
        public float odometer { get; set; }
        public float engineDamage { get; set; }
        public float transmissionDamage { get; set; }
        public float cabinDamage { get; set; }
        public float chassisDamage { get; set; }
        public float wheelDamage { get; set; }
        public float trailerDamage { get; set; }
        public long realTimeStarted { get; set; }
        public long realTimeEnded { get; set; }
        public long realTimeTaken { get; set; }
    }
}
