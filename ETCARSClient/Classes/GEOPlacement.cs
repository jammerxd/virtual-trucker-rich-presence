﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class GEOPlacement
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
        public float heading { get; set; }
        public float pitch { get; set; }
        public float roll { get; set; }
    }
}
