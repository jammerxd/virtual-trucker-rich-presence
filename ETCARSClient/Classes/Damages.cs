﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Damages
    {
        public float engine { get; set; }
        public float transmission { get; set; }
        public float cabin { get; set; }
        public float chassis { get; set; }
        public float wheels { get; set; }
        public float trailer { get; set; }
    }
}
