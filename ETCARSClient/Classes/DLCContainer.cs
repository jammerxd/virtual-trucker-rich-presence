﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class DLCContainer
    {
        public List<DLC> DLC { get; set; }
        public DLCContainer()
        {
            this.DLC = new List<DLC>();
        }
    }
}
