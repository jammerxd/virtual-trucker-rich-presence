﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Placement
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }
    }
}
