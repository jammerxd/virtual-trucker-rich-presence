﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Job
    {
        public string cargoID { get; set; }
        public string cargo { get; set; }
        public float mass { get; set; }
        public float income { get; set; }
        public string destinationCityID { get; set; }
        public string destinationCity { get; set; }
        public string destinationCompanyID { get; set; }
        public string destinationCompany { get; set; }
        public string sourceCityID { get; set; }
        public string sourceCity { get; set; }
        public string sourceCompanyID { get; set; }
        public string sourceCompany { get; set; }
        public int deliveryTime { get; set; }
        public bool isLate { get; set; }
        public int timeRemaining { get; set; }
    }
}
