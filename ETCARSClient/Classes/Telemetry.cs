﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Telemetry
    {
        public string status { get; set; }
        public PluginVersion pluginVersion { get; set; }
        public Game game { get; set; }
        public Truck truck { get; set; }
        public Navigation navigation { get; set; }
        public Job job { get; set; }
        public Trailer trailer { get; set; }
        public User user { get; set; }
        public JobData jobData { get; set; }

        public Telemetry()
        {
            this.pluginVersion = new PluginVersion();
            this.game = new Game();
            this.truck = new Truck();
            this.trailer = new Trailer();
            this.navigation = new Navigation();
            this.user = new User();
            this.job = new Job();
            this.jobData = new JobData();
            
        }
    }
}
