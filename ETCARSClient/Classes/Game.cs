﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Game
    {
        public bool isMultiplayer { get; set; }
        public bool paused { get; set; }
        public bool isDriving { get; set; }
        public int majorVersion { get; set; }
        public int minorVersion { get; set; }
        public string gameID { get; set; }
        public string gameName { get; set; }
        public string gameVersionStr { get; set; }
        public string gameVersionOnly { get; set; }
        public uint gameDateTime { get; set; }
        public int nextRestStop { get; set; }
        public string gameDayTime { get; set; }
        public string gameDateTimeStr { get; set; }
        public string osEnvironment { get; set; }
        public string architecture { get; set; }
        public float localScale { get; set; }
        public List<string> substances { get; set; }

        public Game()
        {
            this.substances = new List<string>();
        }
    }
}
