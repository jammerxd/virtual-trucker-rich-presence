﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class PluginVersion
    {
        public int majorVersion { get; set; }
        public int minorVersion { get; set; }
        public string pluginVersionOnlyStr { get; set; }
    }
}
