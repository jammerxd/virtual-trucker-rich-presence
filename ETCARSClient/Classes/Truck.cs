﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETCARSClient.Classes
{
    public class Truck
    {
        public float cruiseControlSpeed { get; set; }
        public int gear { get; set; }
        public int gearDisplayed { get; set; }
        public uint retarderBrakeLevel { get; set; }
        public bool wipersOn { get; set; }
        public string make { get; set; }
        public string makeID { get; set; }
        public string model { get; set; }
        public string modelID { get; set; }
        public string shifterType { get; set; }
        public float odometer { get; set; }
        public bool hasTruck { get; set; }
        public bool engineEnabled { get; set; }
        public bool electricsEnabled { get; set; }
        public bool motorBrake { get; set; }
        public bool parkingBrake { get; set; }
        public float speed { get; set; }
        public float engineRPM { get; set; }
        public float brakeTemperature { get; set; }
        public float fuelRange { get; set; }
        public float oilPressure { get; set; }
        public float oilTemperature { get; set; }
        public float waterTemperature { get; set; }
        public float batteryVoltage { get; set; }
        public float inputSteering { get; set; }
        public float inputThrottle { get; set; }
        public float inputBrake { get; set; }
        public float inputClutch { get; set; }
        public float effectiveSteering { get; set; }
        public float effectiveThrottle { get; set; }
        public float effectiveBrake { get; set; }
        public float effectiveClutch { get; set; }
        public uint hShifterSlot { get; set; }
        public float brakeAirPressure { get; set; }
        public float adBlue { get; set; }
        public float dashboardBacklight { get; set; }
        public uint forwardGearCount { get; set; }
        public uint reverseGearCount { get; set; }
        public uint retarderStepCount { get; set; }
        public bool trailerConnected { get; set; }
        public GEOPlacement worldPlacement { get; set; }
        public Placement linearVelocity { get; set; }
        public Placement angularVelocity { get; set; }
        public Placement linearAcceleration { get; set; }
        public Placement angularAcceleration { get; set; }
        public GEOPlacement cabinOffset { get; set; }
        public Placement hookPosition { get; set; }
        public Placement headPosition { get; set; }
        public Placement cabinAngularVelocity { get; set; }
        public Placement cabinAngularAcceleration { get; set; }
        public List<float> forwardRatios { get; set; }
        public List<float> reverseRatios { get; set; }
        public int wheelCount { get; set; }
        public List<WheelInfo> wheelInfo { get; set; }
        public GEOPlacement trailerPlacement { get; set; }
        public Warnings warnings { get; set; }
        public Damages damages { get; set; }
        public Lights lights { get; set; }
        public Fuel fuel { get; set; }

        public Truck()
        {
            this.angularAcceleration = new Placement();
            this.angularVelocity = new Placement();
            this.cabinAngularAcceleration = new Placement();
            this.cabinAngularVelocity = new Placement();
            this.cabinOffset = new GEOPlacement();
            this.damages = new Damages();
            this.forwardRatios = new List<float>();
            this.fuel = new Fuel();
            this.headPosition = new Placement();
            this.hookPosition = new Placement();
            this.lights = new Lights();
            this.linearAcceleration = new Placement();
            this.linearVelocity = new Placement();
            this.reverseRatios = new List<float>();
            this.trailerPlacement = new GEOPlacement();
            this.warnings = new Warnings();
            this.wheelInfo = new List<WheelInfo>();
            this.worldPlacement = new GEOPlacement();
        }
    }
}
