﻿using ETCARSClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ETCARSClient.Classes;
using DiscordRPC;
using DiscordRPC.Logging;
using System.Diagnostics;

namespace Virtual_Trucker_Rich_Presence
{
    public class VTRPC_ETCARS_CLIENT : IETCARSClient
    {
        public VTRPC_ETCARS_CLIENT()
        {
            
        }
        #region Interface Methods
        public string GetId()
        {
            return "VT-RPC-Official";
        }
        public void OnReceive(string data)
        {
            dynamic dataobj = JsonConvert.DeserializeObject(data);
            if(dataobj != null)
            {
                try
                {
                    if(dataobj["data"]["status"].ToString() == "TELEMETRY")
                    {
                        Data telemetry = JsonConvert.DeserializeObject<Data>(data);

                        if(telemetry != null)
                        {
                            InitializeDiscordRPCClienct(telemetry, lastGameReported != telemetry.data.game.gameID);
                            lastGameReported = telemetry.data.game.gameID;
                            SetDiscordRPC(telemetry);
                        }
                    }
                }
                catch(Exception ex)
                {

                }
            }
        }
        #endregion


        #region Discord RPC Items
        private DiscordRpcClient discordClient;
        private bool discordClientInitialized;
        private string lastGameReported = "";
        private void SetDiscordRPC(Data telemetry)
        {
            
            if(discordClient != null)
            {
                if(discordClient.IsInitialized && !discordClient.IsDisposed)
                {
                    RichPresence rpc = new RichPresence();
                    rpc.Assets = new Assets();
                    rpc.Details = string.Empty;
                    rpc.Party = new Party();
                    rpc.Secrets = new Secrets();
                    rpc.State = string.Empty;
                    rpc.Timestamps = new Timestamps();



                    discordClient.SetPresence(rpc);
                    discordClient.Invoke();
                }
            }

            
        }
        private void InitializeDiscordRPCClienct(Data telemetry,bool reset=false)
        {
            if (discordClientInitialized)
            {
                if (!reset)
                    return;
                else
                {
                    discordClient.Deinitialize();
                    discordClient.Dispose();
                    discordClient = null;
                    discordClientInitialized = false;
                }
            }

            discordClient = new DiscordRpcClient(telemetry.data.game.gameID == "ats" ? Config.Applications.ATS : Config.Applications.ETS2);
            discordClient.Logger = new ConsoleLogger() { Level = LogLevel.Warning };

            //Subscribe to events
            discordClient.OnReady += (sender, e) =>
            {
                Debug.WriteLine("Received Ready from user {0}", e.User.Username);
            };

            discordClient.OnPresenceUpdate += (sender, e) =>
            {
                Debug.WriteLine("Received Update! {0}", e.Presence);
            };

            //Connect to the RPC
            discordClient.Initialize();
            discordClientInitialized = true;
        }

        public void Dispose()
        {
            discordClient.Deinitialize();
            discordClient.Dispose();
        }
        #endregion
    }
}
