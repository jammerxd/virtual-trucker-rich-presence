﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Virtual_Trucker_Rich_Presence
{
    public partial class VirtualTruckerRichPresenceService : ServiceBase
    {
        private Timer ServiceTimer { get; set; }
        private List<string> SupportedGames { get; set; }
        private bool StopRequested { get; set; }

        private ETCARSClient.ETCARSClient Client = new ETCARSClient.ETCARSClient();
        

        public VirtualTruckerRichPresenceService()
        {
            InitializeComponent();
            SupportedGames = new List<string>();
            SupportedGames.Add("amtrucks");
            SupportedGames.Add("eurotrucks2");
            SupportedGames.Add("American Truck Simulator");
            SupportedGames.Add("Euro Truck Simulator 2");
            ServiceTimer = new Timer();
            ServiceTimer.Enabled = true;
            ServiceTimer.Interval = 5000;
            ServiceTimer.AutoReset = true;
            ServiceTimer.Elapsed += ServiceTimer_Elapsed;
            StopRequested = false;
            Client.AddCallback(new VTRPC_ETCARS_CLIENT());
        }

        private void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if(IsGameRunning() && !StopRequested)
            {
                if(!Client.Connected)
                {
                    try
                    {
                        Client.Connect();
                    }
                    catch(Exception ex)
                    {

                    }
                }

            }
            else if(StopRequested)
            {
                if(Client.Connected)
                {
                    try
                    {
                        Client.Disconnect();
                    }
                    catch(Exception ex)
                    {

                    }
                }
            }
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            StopRequested = false;
            ServiceTimer.Enabled = true;
            ServiceTimer.Start();  
        }

        protected override void OnStop()
        {
            StopRequested = true;
            ServiceTimer.Enabled = false;
            ServiceTimer.Stop();
            base.OnStop();
        }

        protected override void OnPause()
        { 
            base.OnPause();
            StopRequested = true;
            ServiceTimer.Enabled = false;
            ServiceTimer.Stop();
              
        }

        protected override void OnContinue()
        {
            base.OnContinue();
            StopRequested = false;
            ServiceTimer.Enabled = true;
            ServiceTimer.Start();
        }

        protected override void OnShutdown()
        {
            base.OnShutdown();
            StopRequested = true;
            ServiceTimer.Enabled = false;
            ServiceTimer.Stop();
        }


        private bool IsGameRunning()
        {
            foreach(var p in Process.GetProcesses())
            {
                foreach(var g in SupportedGames)
                {
                    if(p.ProcessName.StartsWith(g,StringComparison.OrdinalIgnoreCase))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
