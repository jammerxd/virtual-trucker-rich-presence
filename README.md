<img src="https://i.sgtbrds.tk/oge4fn.png" width="768px" />

# Version 3.0.0

An easy plugin to let others see your job, truck, etc. on Discord Rich Presence.  
Using a Windows C# service to collect data from ETCars to send information to Discord.  
While being more stable than our NodeJS varient, it won't be 100% reliable.  

Here's our Discord server for support: https://discord.gg/Zt49WDH  

## Things to note!
* TruckersMP has their own Rich Presence which needs to be disabled in MP Settings!
* ProMods information is only supported on Multiplayer

## Rich Presence Examples
### Rich Presence on Singleplayer
<img src="https://via.placeholder.com/540x150" />

### Rich Presence on Multiplayer
<img src="https://via.placeholder.com/540x150" />

### Rich Presence on Multiplayer (ProMods)
<img src="https://via.placeholder.com/540x150" />